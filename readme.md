Emojicrypt
=============

Emojicrypt is a super secure application to store secret messages. It holds that since emoji have multiple bytes, whereas ascii character only have a single byte, then a password composed entirely of emoji must be doubly as secure.

The Task
-------------

Implement a more rich frontend for this Laravel application. The frontend should have the following features:

* An emoji-picker. The user should be able to click a button and have a picker popup with several different emoji, or the user should be able to type a shortcode (eg. `:thumbsup:`) and have it replaced with an Emoji (👍).
* Any other quality-of-life, design, or usability features you think would make the tool better.

You may use any tools, languages or frameworks that you want with the sole caveat that while Android and MacOS do have built-in emoji pickers, assume they do not. Build something in Javascript.

Setting up the app
-------------------

You'll need a mysql service running on localhost, and to create a database called `emojicrypt`.

Copy `.env.example` to `.env` and update the `DB_USERNAME` and `DB_PASSWORD` variables.

Run `php artisan migrate` to create the initial table schema.

You can run `php artisan serve` to access a built-in server on `localhost:8000`.

Notes
-------

This app was quickly scaffolded as a means to help you out so you can show off your frontend skills without re-implementing the cruft in the backend. You are free to make any changes you want, or throw the whole thing out and rebuild the feature-set yourself.

In the same vein, it's not very well tested, and you may find edge-cases.

The controllers _should_ respond to AJAX calls out of the box, with routes defined in `routes/web.php`. Again, you might need to fix bugs if you encounter them.

Believe in yourself.
