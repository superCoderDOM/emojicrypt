<?php /** @var \App\Message[] $data */ ?>
@extends('layouts.master')

@section('title', 'All Messages')

@section('content')

    @if(Auth::check())

        @include('layouts.userDashboard', [ 'messages_count' => count($data['messages']) ])

    @endif

    <div class="my-3 p-3 bg-white rounded box-shadow">

        <h3 class="border-bottom border-gray pb-2 mb-0">New message</h3>

        @if(Auth::check())

            @include('message.new')

        @else

            <em>You must be signed in to create a new message!</em>

        @endif

    </div>

    <div class="my-3 p-3 bg-white rounded box-shadow">

        <h3 id="recent-messages" class="border-bottom border-gray pb-2 mb-0">{{ Auth::check() ? '' : 'All' }} Recent messages</h3>

        @forelse ($data['messages'] as $message)

            <div class="media text-muted pt-3">

                <div class="box mr-2 rounded"></div>

                <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">

                    @if(Auth::check())
                        @if($message->from_user_id != Auth::user()->id)

                            <p>From <strong class="text-gray-dark">{{ $data['users'][($message->from_user_id) - 1]->name }}</strong>, {{ $message->created_at->diffForHumans() }}</p>

                        @else

                            <p>To <strong class="text-gray-dark">{{ $data['users'][($message->to_user_id) - 1]->name }}</strong>, {{ $message->created_at->diffForHumans() }}</p>

                        @endif

                    @else

                        <p>From <strong class="text-gray-dark">{{ $data['users'][($message->from_user_id) - 1]->name }}</strong> to <strong>{{ $data['users'][($message->to_user_id) - 1]->name }}</strong>, {{ $message->created_at->diffForHumans() }}</p>

                    @endif

                    <div>

                        @include('message.decrypt', [ 'message_id' => $message->id ])

                    </div>

                </div>

            </div>

        @empty

            <em>No messages, create a new message!</em>

        @endforelse

        <small class="d-block text-right mt-3">

            <a href="#">All messages</a>

        </small>

    </div>

@stop

@section('scripts')

    <!-- EmojiOne Area Picker -->
    <script type="text/javascript">

        $(document).ready(function() {
            $("#encrypt-emoji-key").emojioneArea();
            $("#message").emojioneArea();
            @if (isset($data['messages']))
                @foreach ($data['messages'] as $message)
                    $("#decrypt-emoji-key-{{ $message->id }}").emojioneArea();
                @endforeach
            @endif
        });

    </script>

@stop
