@extends('layouts.master')

@section('title', "View Message")

@section('content')

        <div class="my-3 p-3 bg-white rounded box-shadow">
    
            <h3 class="border-bottom border-gray pb-2 mb-0">Message from {{ $decrypted_message->message->from }}</h3>

            <div>
                {{ $decrypted_message->plaintext }}
            </div>

        </div>

@stop
