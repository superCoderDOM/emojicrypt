<form action="{{ URL::route('messages.decrypt', $message_id) }}" method="post">
    @csrf

    <div class="form-row">

        <div class="col-12 col-md-8 mb-2">
            <label for="emoji-key" class="sr-only">Emoji Key</label>
            <input 
                type="text" 
                id="decrypt-emoji-key-{{ $message_id }}"
                name="emoji-key" 
                class="form-control" 
                aria-describedby="emojiKeyHelp" 
                placeholder="Enter secret emoji key"
                required />
            <small id="emojiKeyHelp" class="form-text text-muted">The original emoji key used to encrypt this message is required to decrypt it.</small>
        </div>

        <div class="col-12 col-md-2">
            <input type="submit" class="btn btn-secondary" value="Decrypt Message" />
        </div>

    </div>

    @include('layouts.errors')

</form>