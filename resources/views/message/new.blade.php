<form action="{{ URL::route('messages.encrypt') }}" method="post" class="mt-2">
    @csrf

    <div class="form-group">

        <label for="to_user" class=""><strong>To</strong></label>
        <input list="users" name="to_user" id="to_user" class="form-control" placeholder="Name of recipient" required>
        <datalist id="users">

            @foreach($data['users'] as $user)

                @if($user->id != Auth::user()->id)
                    <option value="{{ $user->name }}">
                @endif

            @endforeach

        </datalist>

    </div>
    
    <div class="form-group">
        <label for="message" class=""><strong>Message</strong></label>
        <textarea name="message" class="form-control" id="message" rows="4" placeholder="Your message" required></textarea>
    </div>

    <label for="emoji-key"><strong>Emoji Key</strong></label>
    <div class="form-row">

        <div class="col-12 col-md-8 mb-2">
            <input 
                type="text" 
                name="emoji-key" 
                id="encrypt-emoji-key" 
                class="form-control" 
                aria-describedby="emojiNewKeyHelp" 
                placeholder="Create secret emoji key"
                required />
            <small id="emojiNewKeyHelp" class="form-text text-muted">This emoji key will be required to decrypt the message.</small>
        </div>
    
        <div class="col-12 col-md-2">
            <input type="submit" class="btn btn-secondary" value="Encrypt Message" />
        </div>

    </div>

    @include('layouts.errors')

</form>