@extends('layouts.master')

@section('title', 'Login')

@section('content')

    <div class="my-3 p-3 bg-white rounded box-shadow">

        <h1 class="border-bottom border-gray pb-2 mb-0">Sign In</h1>

        <form method="POST" action="/login" class="mt-2">
            @csrf

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-secondary">Sign In</button>
            </div>

            <div class="form-group">
                @include('layouts.errors')
            </div>

        </form>

    </div>

@stop