@extends('layouts.master')

@section('title', 'Register')

@section('content')

    <div class="my-3 p-3 bg-white rounded box-shadow">

        <h1 class="border-bottom border-gray pb-2 mb-0">Register</h1>

        <form method="POST" action="/register" class="mt-2">
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="form-group">
                <label for="password_confirmation">Password Confirmation</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-secondary">Register</button>
            </div>

            <div class="form-group">
                    @include('layouts.errors')
            </div>

        </form>

    </div>

@stop