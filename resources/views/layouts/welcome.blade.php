@extends('layouts.master')

@section('title', 'Welcome')

@section('jumbotron')

    <div class="jumbotron jumbotron-fluid">

        <div class="container">

            <h1 class="display-4">Emojicrypt</h1>
            <p class="lead">A super secure application to store secret messages</p>

        </div>

    </div>

@stop