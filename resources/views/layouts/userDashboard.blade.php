<div class="d-flex align-items-center justify-content-between p-3 my-3 text-white-50 bg-orange rounded box-shadow">

<div class="d-flex align-items-center">
    <img class="mr-3" src="/zipper_mouth.png" alt="" width="48" height="48">

    <div class="lh-100">
    <h6 class="mb-0 text-white lh-100">{{ Auth::user()->name }}</h6>
        <small>Member since {{ Auth::user()->created_at->format('Y') }}</small>
    </div>
</div>
    <div class="">
        <a class="nav-link text-white" href="#recent-messages">
            Messages
            <span class="badge badge-pill bg-light align-text-bottom badge-text">{{ $messages_count }}</span>
        </a>
    </div>

</div>