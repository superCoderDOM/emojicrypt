<!doctype html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Emojicrypt is a super secure application to store secret messages.">
        <meta name="author" content="Whitespark.ca">
        <link rel="icon" href="/zipper_mouth.png">

        <title>Emojicrypt - @yield('title')</title>

        <!-- Custom CSS -->
        <link href="/css/app.css" rel="stylesheet">

    </head>

    <body class="bg-light">

        @include('layouts.nav')

        @yield('jumbotron')
        
        <main role="main">

            <div  class="container">

                @yield('content')

            </div>

        </main>

        @include('layouts.footer')

        <!-- Bootstrap core JavaScript -->
        <script src="/js/app.js"></script>
        <script src="/js/offcanvas.js"></script>
        <script src="/js/emojione.js"></script>
        <script src="/js/emojionearea.js"></script>

        @yield('scripts')

    </body>

</html>