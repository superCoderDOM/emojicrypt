<footer class="footer mt-4">
    <div class="container text-muted">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
        <p>Emojicrypt &copy; Whitespark {{ date("Y") }}</p>
        <p>New to Emojicrypt? <a href="/register">Create a FREE account</a>.</p>
    </div>
</footer>
