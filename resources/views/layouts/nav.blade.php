<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <a class="navbar-brand" href="/">Emojicrypt</a>

    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarDefault">

        <ul class="navbar-nav mr-auto justify-content-between align-items-center">

            @if(Auth::check())

                <li class="nav-item active">
                    <a class="nav-link" href="/message/{{ Auth::user()->id }}">Dashboard <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                    <div class="dropdown-menu" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="/logout">Logout</a>
                    </div>
                </li>

            @else
                <li class="nav-item active">
                    <a class="nav-link" href="/message">Message Board<span class="sr-only">(current)</span></a>
                </li>

                <li class="d-flex align-items-stretch">
                <div class="nav-item">
                    <a class="nav-link" href="/register">
                        <button class="btn btn-outline-secondary">Register</button>
                    </a>
                </div>

                <div class="nav-item">
                    <a class="nav-link" href="/login">
                        <button class="btn btn-outline-secondary">Login</button>
                    </a>
                </div>
            </li>
            @endif

        </ul>

    </div>

</nav>