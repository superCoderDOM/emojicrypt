<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', function () {
    return view('layouts.welcome');
})->name('home');

// Message Routes
Route::get('/message', 'MessageController@showAll')->name('messages.list');
Route::post('/message', 'MessageController@encrypt')->name('messages.encrypt');
Route::get('/message/{user_id}', 'MessageController@showUser')->name('messages.userList');
Route::post('/message/{id}/decrypt', 'MessageController@decrypt')->name('messages.decrypt');

// Registration Routes
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

// Authorization Routes
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');