<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class RegistrationController extends Controller
{
    //

    public function create()
    {
        return view('registration.create');
    }

    public function store()
    {
        // Validate the form
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        try {
            // Create and save user
            $user = User::create(request(['name', 'email', 'password']));

            // Sign user in
            auth()->login($user);

            // Redirect to home page
            return redirect()->to('message/'.$user);

        } catch (User $exception) {

            return back()->withErrors([
                'message' => 'Something went wrong'
            ]);
        }

        // // Create and save user
        // $user = User::create(request(['name', 'email', 'password']));

        // // Sign user in
        // auth()->login($user);

        // // Redirect to home page
        // return redirect()->home();

    }
}
