<?php

namespace App\Http\Controllers;

use App\Http\DecryptedMessage;
use App\Http\Requests\DecryptMessageRequest;
use App\Http\Responses\DecryptionFailedResponse;
use App\Http\Responses\MessageDecryptedResponse;
use App\Http\Responses\MessageEncryptedResponse;
use App\User;
use App\Message;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['showAll']);
    }

    public function showAll()
    {
        $data['messages'] = Message::query()->orderByDesc('id')->get();
        $data['users'] = User::select('id', 'name')->get();

        return view('message.list')->with('data', $data);
    }

    public function showUser($user_id)
    {
        $data['messages'] = Message::query()
            ->where('from_user_id', $user_id)
            ->orWhere('to_user_id', $user_id)
            ->orderByDesc('id')
            ->get();
        
        $data['users'] = User::select('id', 'name')->get();

        return view('message.list')->with('data', $data);
    }

    public function encrypt(Request $request)
    {

        $this->validate(request(), [
            'to_user' => 'required',
            'message' => 'required|min:2',
            'emoji-key' => 'required'
            ]);

        $from = \Auth::user()->name;
        $from_user_id = auth()->id();
        $to_user_id = User::select('id')->where('name', $request->get('to_user'))->get()->first()['id'];
        $message = $request->get('message');
        $key = $request->get('emoji-key');


        $message = Message::encryptWithKey($from, $from_user_id, $to_user_id, $message, $key);

        return new MessageEncryptedResponse($message);
    }

    public function decrypt(DecryptMessageRequest $request, $id)
    {

        $this->validate(request(), ['emoji-key' => 'required']);

        /** @var Message $message */
        $message = Message::query()->findOrFail($id);

        try {
            $decrypted = new DecryptedMessage($message, $request->get('emoji-key'));

            return new MessageDecryptedResponse($decrypted);
        } catch (DecryptException $exception) {
            return new DecryptionFailedResponse();
        }
    }

}
