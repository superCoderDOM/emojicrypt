<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }

    public function create()
    {
        return view('session.create');
    }

    public function store()
    {
        // Attempt to authorize user
        if (! auth()->attempt(request(['email', 'password']))) {

            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);

        }

        // Redirect to home page
        return redirect()->to('message/'.\Auth::user()->id);
    }

    public function destroy()
    {
        auth()->logout();

        return redirect()->home();
    }
}
