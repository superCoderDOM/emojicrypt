<?php

namespace App\Http\Responses;

use App\Message;
use Illuminate\Contracts\Support\Responsable;

class MessageEncryptedResponse implements Responsable
{

    /**
     * @var Message
     */
    private $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function toResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([ 'success' => true, 'message' => $this->message ]);
        }

        return redirect()->to('message/'.\Auth::user()->id);
    }
}
