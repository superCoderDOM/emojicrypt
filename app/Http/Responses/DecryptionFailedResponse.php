<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;

class DecryptionFailedResponse implements Responsable
{

    public function toResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([ 'success' => false, 'message' => null ]);
        }

        return redirect()->route('messages.list')->withErrors("Error decrypting message. Wrong key?");
    }
}
