<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Crypt;

/**
 * @property string message
 */
class Message extends Model
{

    protected $guarded = [];

    public static function encryptWithKey($from, $from_user_id, $to_user_id, $message, $key)
    {
        $key = self::generateKey($key);

        return self::create([
            'from' => $from,
            'from_user_id' => $from_user_id,
            'to_user_id' => $to_user_id,
            'message' => (new Encrypter($key))->encryptString($message),
        ]);
    }

    public static function generateKey($key)
    {
        return str_pad(str_limit(md5($key), 16, ''), 16, ' ');
    }

    public function from_user()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function to_user()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

}
